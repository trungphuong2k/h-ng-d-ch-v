# Tiện ích hỗ trợ sinh viên phúc khảo

## Giới thiệu

Dựa theo nhu cầu thực tế về việc đẩy mạnh sử dụng công nghệ, cũng như giảm bớt những khó khăn của nhân viên nhà trường và thắc mắc của sinh viên trong các quy trình xử lí giấy tờ hành chính, nhóm chúng mình có ý tưởng xây dựng tiện ích hỗ trợ sinh viên phúc khảo. Tiện ích này hỗ trợ các chức năng như:

  - Cung cấp mẫu đơn có sắn các lựa chọn cho sinh viên nhằm đơn giản hóa việc làm đơn phúc khảo.
  
  - Cho phép người dùng theo dõi trạng thái của đơn phúc khảo. 
  
  - Nhận mail thông báo khi đơn phúc khảo được cập nhật trạng thái

  - Hỗ trợ thanh toán qua Paypal

## Các công nghệ sử dụng

- Backend: SpringBoot, RESTful API, JPA

- Database: Mysql

- Server: Ubuntu, Docker

- Frontend:  Reactjs, Axios, Bootstrap, Antd, MailJS

- IDE: IntelliJ, VSCode

## Đóng Góp

- Giảng viên: Đặng Ngọc Hùng

- Sinh Viên:

  - Lê Thị Minh Thư

  - Trung Thị Phương

  - Hồ Sỹ Lâm

Rất mong nhận được các đóng góp từ mọi người.

