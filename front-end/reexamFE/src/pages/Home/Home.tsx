import { useEffect, useState, useRef, LegacyRef } from "react";
import "./MainLayout.scss";
import "./HabitList.scss";
import "./Home.scss";
import axiosClient from "../../utils/axiosClient";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Dropdown,
  DropdownButton,
  Table,
  Modal,
  Button,
  Form,
} from "react-bootstrap";
import emailjs from "emailjs-com";

export default function Home() {
  document.title = "Danh sách đơn phúc khảo";

  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [currExam, setCurrExam] = useState<any>();
  const scoreRef = useRef<HTMLInputElement>();

  const Status: any = {
    ACCEPT: "Chưa tiếp nhận",
    accepted: "Đã tiếp nhận",
    rejected: "Đã từ chối",
    finished: "Đã hoàn thành",
  };

  const handleClose = () => setShow(false);
  const handleShow = (exam: any) => {
    setShow(true);
    setCurrExam(exam);
  };

  const handleClose2 = () => setShow2(false);
  const handleShow2 = (exam: any) => {
    setCurrExam(exam);
    setShow2(true);
  };

  const [exams, setExams] = useState<any>([]);

  useEffect(() => {
    dispatchExams();
  }, []);

  function dispatchExams() {
    axiosClient
      .get("/reexaminations/all")
      .then((res) => {
        setExams(res);
      })
      .catch((error) => {
        console.log(error.toJSON());
      });
  }

  function sendEmail(exam: any, status: any, result: any) {
    const templateParams = {
      subject: "Trạng thái phúc khảo",
      name: `Trạng thái phúc khảo của sinh viên ${exam.transcriptLine.student.student_code} - ${exam.transcriptLine.student.name}`,
      message: `${Status[status]} phúc khảo môn ${exam.transcriptLine.study_class.subject_semester.subject.name} - ${exam.transcriptLine.study_class.subject_semester.subject.subjectCode} ${result}`,
      to_email: "thichluudan@gmail.com",
      // to_email: exam.transcriptLine.student.email,
    };

    emailjs
      .send(
        "service_p7p9n6l",
        "template_evmi2in",
        templateParams,
        "Fjk75VnhJx-J30WU8"
      )
      .then((res) => {
        console.log(res.text);
      })
      .catch((error) => {
        console.log(error.text);
      });
  }

  function handleRequest(exam: any, status: string) {
    let result: any = "";

    if (status === "finished") {
      if (
        ~~scoreRef.current!.value === exam.point ||
        !scoreRef.current!.value
      ) {
        result = "với điểm số không thay đổi";
      } else {
        result = `với điểm số mới là  ${scoreRef.current?.value}`;
      }
    }

    axiosClient
      .put("/reexaminations", {
        id: exam.id,
        status: status,
        point:
          status === "finished" && scoreRef.current!.value !== ""
            ? scoreRef.current?.value
            : exam.point,
        submit_time: exam.submit_time,
      })
      .then(() => {
        sendEmail(exam, status, result);
        dispatchExams();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <div className="main-layout">
      <div className="children ">
        <h3 className="header">Danh sách đơn phúc khảo</h3>
        <div className="container">
          <Table
            bordered
            style={{ background: "#f7f7f7eb", borderRadius: "10px" }}
          >
            <thead>
              <tr>
                <th>STT</th>
                <th>Thời gian gửi</th>
                <th>Mã SV - Họ tên</th>
                <th>Môn - Mã môn</th>
                <th>Trạng thái</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {exams.map((exam: any, index: number) => (
                <tr key={index}>
                  <td>{index}</td>
                  <td>{exam.submit_time}</td>
                  <td>
                    {exam.transcriptLine.student.student_code} -{" "}
                    {exam.transcriptLine.student.name}
                  </td>
                  <td>
                    {
                      exam.transcriptLine.study_class.subject_semester.subject
                        .name
                    }{" "}
                    -{" "}
                    {
                      exam.transcriptLine.study_class.subject_semester.subject
                        .subjectCode
                    }
                    <br />
                    {exam.transcriptLine.study_class.name}
                  </td>
                  <td>
                    {Status[exam.status]}
                    <DropdownButton
                      id="dropdown-basic-button"
                      title="Cập nhật trạng thái"
                      disabled={exam.status === "finished" ? true : false}
                    >
                      <Dropdown.Item
                        as="button"
                        onClick={() => handleRequest(exam, "accepted")}
                      >
                        Đã tiếp nhận
                      </Dropdown.Item>
                      <Dropdown.Item
                        as="button"
                        onClick={() => handleRequest(exam, "rejected")}
                      >
                        Đã từ chối
                      </Dropdown.Item>
                      <Dropdown.Item
                        as="button"
                        onClick={() => {
                          handleShow(exam);
                        }}
                      >
                        Đã hoàn thành phúc khảo
                      </Dropdown.Item>
                    </DropdownButton>
                  </td>
                  <td>
                    <br />
                    <Button
                      onClick={() => {
                        handleShow2(exam);
                      }}
                    >
                      Xem chi tiết
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>

          <Modal show={show} onHide={handleClose} centered>
            <Modal.Header closeButton>
              <Modal.Title>Nhập điểm mới sau phúc khảo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group className="mb-3">
                  <Form.Control
                    // @ts-ignore
                    ref={scoreRef as LegacyRef<HTMLInputElement>}
                    type="text"
                    placeholder="Bỏ trống nếu điểm không thay đổi"
                    autoFocus
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Hủy
              </Button>
              <Button
                variant="primary"
                onClick={() => {
                  handleRequest(currExam, "finished");
                  handleClose();
                }}
              >
                Cập nhật
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={show2} onHide={handleClose2} centered>
            <Modal.Header closeButton>
              <Modal.Title>Nhập điểm mới sau phúc khảo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group className="mb-3">
                  <Form.Control
                    // @ts-ignore
                    ref={scoreRef as LegacyRef<HTMLInputElement>}
                    type="text"
                    placeholder="Bỏ trống nếu điểm không thay đổi"
                    autoFocus
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Hủy
              </Button>
              <Button
                variant="primary"
                onClick={() => {
                  handleRequest(currExam, "finished");
                  handleClose();
                }}
              >
                Cập nhật
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={show2} onHide={handleClose2} centered>
            <Modal.Header closeButton>
              <Modal.Title>Chi tiết đơn phúc khảo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>
                <b>Thời gian gửi: </b> {currExam?.submit_time}
              </p>
              <p>
                <b>Mã SV:</b> {currExam?.transcriptLine.student.student_code}
              </p>
              <p>
                <b>Họ tên:</b> {currExam?.transcriptLine.student.name}{" "}
              </p>
              <p>
                <b>Email:</b> {currExam?.transcriptLine.student.mail}{" "}
              </p>
              <p>
                <b> Môn phúc khảo:</b>{" "}
                {
                  currExam?.transcriptLine.study_class.subject_semester.subject
                    .name
                }
                {" - "}
                {
                  currExam?.transcriptLine.study_class.subject_semester.subject
                    .subjectCode
                }
              </p>
              <div>
                <b>Điểm: </b>
                {currExam?.transcriptLine.transcript_item.map(
                  (item: any, index: any) => (
                    <p key={index}>
                      {item.subjectPoint.point.name}: {item.point}
                    </p>
                  )
                )}
              </div>
              <p>
                <b>Trạng thái:</b> {Status[currExam?.status]}{" "}
              </p>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose2}>
                Đóng
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    </div>
  );
}
