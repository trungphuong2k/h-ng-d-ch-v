import axios from "axios";
import queryString from "query-string";

const axiosClient = axios.create({
  baseURL: "http://8.219.43.224:8081/api/v1",
  headers: {
    "content-type": "application/json",
  },
  timeout: 10000,
  paramsSerializer: (params) => queryString.stringify(params),
});

axiosClient.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    throw error;
  }
);

axiosClient.interceptors.response.use(
  (response) => {
    if (response && response.data) return response.data;
  },
  (error) => {
    throw error;
  }
);

export default axiosClient;
