import { useState } from "react";
import Modal from "react-modal";

import "react-toastify/dist/ReactToastify.css";
import "./HabitModal.scss";

// interface IHabit {
//   id?: number;
//   title: string;
//   description: string;
//   reminderTime: Date | string;
//   reminderDays: number[];
//   performances?: { time: string; isChecked: boolean }[];
//   createdAt?: Date;
// }

// interface IHabitModalProps {
//   habit: IHabit;
//   habitList: IHabit[];
//   isEditMode?: boolean;
//   isEditModalOpened: boolean;
//   onAddHabit(newHabit: IHabit, msg: string): void;
//   onEditHabit(id: number, habit: IHabit): void;
//   onCloseModal(): void;
// }

export default function HabitModal(props: any) {
  const [habitModalOpened, setHabitModalOpened] = useState(false);

  const initialValue = {
    title: "",
    description: "",
    reminderDays: [0, 1, 2, 3, 4, 5, 6],
    performances: [],
  };

  const [habit, setHabit] = useState(initialValue);
  const [titleError, setTitleError] = useState("");
  const [desError, setDesError] = useState("");

  function saveHabit() {
    if (!habit.title) {
      setDesError("");
      setTitleError("Title is not left blank!");
    } else if (!habit.description) {
      setTitleError("");
      setDesError("Description is not left blank!");
    } else {
      // props.onAddHabit(newHabit);
    }
    handleCloseModal();
  }

  function handleChange(e: any) {
    const { name, value } = e.target;
    setHabit((prevValue) => {
      return {
        ...prevValue,
        [name]: value,
      };
    });
  }

  function handleCloseModal() {
    setHabitModalOpened(false);
    setHabit(initialValue);
    setTitleError("");
    setDesError("");
  }

  return (
    <Modal
      className="habit-modal"
      overlayClassName="habit-modal-overlay"
      closeTimeoutMS={200}
      isOpen={habitModalOpened}
      onRequestClose={handleCloseModal}
      shouldCloseOnOverlayClick={false}
    >
      <div className="modal-content">
        <h2 className="modal-title">
          {props.isEditMode ? "View Habit" : "Add Habit"}
        </h2>

        <form className="data-fields">
          <div className="name-field">
            <label className="data-label">Title:</label>
            <input
              type="text"
              name="title"
              onChange={handleChange}
              defaultValue={habit.title}
              autoComplete={"off"}
              className={titleError !== "" ? "red-border" : ""}
            />
          </div>

          <div className="description-field">
            <label className="data-label">Description:</label>
            <textarea
              name="description"
              onChange={handleChange}
              defaultValue={habit.description}
              autoComplete={"off"}
              className={desError !== "" ? "red-border" : ""}
            />
          </div>
        </form>

        <footer className="modal-footer">
          {titleError !== "" && (
            <div className="error-message">{titleError}</div>
          )}
          {desError !== "" && <div className="error-message">{desError}</div>}
          <div>
            <button className="btn confirm-btn" onClick={saveHabit}>
              SUBMIT
            </button>
            <button className="btn cancel-btn" onClick={handleCloseModal}>
              CANCEL
            </button>
          </div>
        </footer>
      </div>
    </Modal>
  );
}
