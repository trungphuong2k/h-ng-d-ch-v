import { Button, Modal, Popover, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { getExam } from "../../../api";
import FormSubmit from "./FormSubmit";
import { Timeline } from 'antd';
const changeTV = (item: string) => {
    switch (item) {
        case "accept":
            return "Đã chấp nhận"
            break;
        case "finished":
            return "Đã hoàn tất";
            break;
        case "rejected":
            return "Từ chối";
            break;
        default:
            return "Đã chấp nhận"
    }
}
export default function Form() {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [exam, setExam] = useState<any[]>([]);
    const [submit, setSubmit] = useState<boolean>(false);

    const getallExam = async () => {
        const res = await getExam();
        setExam(res.data)
    }
    useEffect(() => {
        getallExam()
    }, [submit]);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const handleSubmit = () => {
        setSubmit(true)
    };
    const name = () => {
        return localStorage.getItem("name");
    }

    return (
        <div >
            <div style={{ fontWeight: 600, fontSize: 20, color: "#94a3b8" }}>Welcome {name()}</div>
            < Button type="primary" onClick={showModal} >
                Tạo Đơn
            </ Button>
            <Modal
                title="Đơn Phúc Khảo"
                visible={isModalVisible}
                onCancel={handleCancel}
                footer={[<Button key="back" onClick={() => handleCancel}>
                    Cancel
                </Button>,]}
            >
                <>
                    <FormSubmit submit={handleSubmit}></FormSubmit>
                </>
            </Modal>
            <h1 style={{ fontWeight: 600, fontSize: 30, color: "#94a3b8", textAlign: "center" }}>Trạng Thái Đơn </h1>
            <div style={{ display: "flex ", gap: 10, padding: 20, background: "#94a3b8 " }}>
                <div style={{ width: "100px" }}>
                    Mã Môn
                </div>
                <div style={{ width: "400px" }}>
                    Tên Môn
                </div>
                <div style={{ width: "200px" }}>
                    Nhóm
                </div>

                <div style={{ width: "200px" }} >
                    Ngày Nộp
                </div>
                <div style={{ width: "200px" }} >
                    Ngày Trả Kết Qủa
                </div>

                <div style={{ width: "300px" }} >
                    Bảng Điểm
                </div>
                <div style={{ width: "150px" }} >
                    Trạng Thái
                </div>
            </div>
            {
                exam.map((exam) => {
                    return (
                        <div style={{ display: "flex ", gap: 10, padding: 20, background: "#f1f5f9 " }}>
                            <div style={{ width: "100px" }}>
                                {exam.transcriptLine.study_class.subject_semester.subject.subjectCode}
                            </div>
                            <div style={{ width: "400px" }}>
                                {exam.transcriptLine.study_class.subject_semester.subject.name}
                            </div>
                            <div style={{ width: "200px" }}>
                                {exam.transcriptLine.study_class.name}
                            </div>

                            <div style={{ width: "200px" }} >
                                {exam.submit_time}
                            </div>
                            <div style={{ width: "200px" }} >
                                {exam.transcriptLine.reexamination_deadline}
                            </div>
                            <div style={{ width: "300px" }} >
                                Bang diem
                            </div>
                            <div style={{ width: "150px", textTransform: "uppercase", color: "green" }} >
                                <Popover content={< Timeline >
                                    <Timeline.Item color="blue">Đã tiếp nhân  {exam.submit_time} </Timeline.Item>
                                    <Timeline.Item color="red">Đã từ chối</Timeline.Item>
                                    <Timeline.Item color="green">Đã hoàn thành  {exam.transcriptLine.reexamination_deadline}</Timeline.Item>
                                </Timeline>} >
                                    <div> {exam.status} </div>
                                </Popover>

                            </div></div>

                    )
                })
            }



        </div >
    );
}
