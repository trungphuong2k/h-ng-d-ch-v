import React, { useEffect, useState } from 'react'
import { Form, Input, Button, Select, message } from 'antd';
import { getPayment, getSem, getStudyClass, getSubject, postReexam } from '../../../api';
import { buildQueries } from '@testing-library/react';
import { Redirect, Route, Router } from 'react-router';
import { useHistory } from "react-router-dom";

const { Option } = Select;

const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

interface PropsFormSubmit {
    submit?: any
}
export default function FormSubmit(props: PropsFormSubmit) {
    const { submit } = props;
    const [subject, setSubject] = useState<any>("");
    const [semesterId, setSemesterId] = useState<any>("");
    const [monhoc, setMonhoc] = useState<any[]>([]);
    const [semmes, setSemmes] = useState<any[]>([]);
    const [stydyClass, setStudyClass] = useState<any>("");
    const history = useHistory();

    const getKiHoc = async () => {
        const res = await getSem();
        console.log(res, "res")
        //@ts-ignore
        setSemmes(res.data)
    }


    useEffect(() => () => {
        getKiHoc()
    }, []);

    const getsub = async () => {
        const res = await getSubject(semesterId);
        //@ts-ignore
        setMonhoc(res.data)
    }

    const getStudy = async () => {
        const res = await getStudyClass(semesterId, subject);
        //@ts-ignore
        setStudyClass(res.data)
    }
    useEffect(() => {
        if (semesterId) {
            getsub()
        }
    }, [semesterId]);

    useEffect(() => {
        if (subject) {
            getStudy();
        }
    }, [subject]);


    const onSemesisChange = (value: string) => {
        setSemesterId(value);
    };
    const onSubjetChange = (value: string) => {
        setSubject(value);

    };

    const onFinish = async () => {
        await postReexam({
            semesterId: semesterId,
            subjectId: subject,
        }).then(() => {
            message.info("Đã nộp đơn thành công!")
            submit();
            payment();
        })

    };
    const payment = async () => {
        const res = await getPayment();
        window.location.href = res.data.response;
    }
    const compare = () => {
        const date = new Date();
        const deaddate = "22/7/2020"
    }

    return (
        <> <Form {...layout} name="control-ref" onFinish={onFinish}>

            <Form.Item name="Kì Học" label="Kì Học" rules={[{ required: true }]}>

                <Select
                    placeholder="Lựa chọn Kì Học "
                    onChange={onSemesisChange}
                    allowClear
                >
                    <Option value="2">Kì 1 </Option>
                    <Option value="1">Kì 2 </Option>

                </Select>
            </Form.Item>
            <Form.Item name="Môn Học" label="Môn Học" rules={[{ required: true }]}>

                <Select
                    placeholder="Lựa chọn môn học "
                    onChange={onSubjetChange}
                    allowClear
                >
                    {monhoc.map((item) => {
                        return (

                            <Option key={item.id} value={item.id}>{item.subjectCode}-{item.name}</Option>
                        )
                    }
                    )}
                </Select>
            </Form.Item>
            {stydyClass && (
                <Form.Item name="Nhóm" label="Nhóm">
                    <Input disabled defaultValue={stydyClass.name} />

                </Form.Item>
            )}
            {stydyClass && (
                <Form.Item name="Hạn sử nộp form" label="Hạn nộp">
                    <Input disabled defaultValue={stydyClass.name} />
                </Form.Item>
            )}
            <Form.Item name="Hạn sử nộp form" label="Hạn nộp">
                <Button onClick={() => { payment() }}>Thanh Toán</Button>
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form></>
    )
}
