import { LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { Breadcrumb, Button, Layout, Menu } from 'antd';
import * as React from 'react';
import { BrowserRouter, Switch } from "react-router-dom";
import './App2.css';
import Form from './component/Authentication/Form/Form';
import Login from "./component/Authentication/Login/Login";
import Register from "./component/Authentication/Registration/Registraion";
import PrivateRoute from "./Route/PrivateRoute";
import PublicRoute from "./Route/PublicRoute";

const {SubMenu} = Menu;
const {Header, Content, Sider} = Layout;

export const name = () => {
  return localStorage.getItem("name");
}

const App = () => {

  const logoOut = () => {
    localStorage.removeItem("id");
  }


  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <PublicRoute  restricted={true} exact path="/login" Component={Login}/>
          <PublicRoute restricted={true} exact path="/register" Component={Register} />
          <PrivateRoute restricted={true} path="/success" Component={Register} />
          <Layout>
          <Header className="header">
            <div className="logo"/>
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
              <Button onClick={logoOut} type="dashed" style={{float:"right",marginTop:"12px"}}  shape="round" icon={<LogoutOutlined />} size="large">LOG OUT</Button>
            </Menu>
          </Header>
          <Layout>
            <Sider width={200} className="site-layout-background">
              <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{height: '100%', borderRight: 0}}
              >
                  <SubMenu key="sub1" icon={<UserOutlined />} title={name()}>
                  <Menu.Item key="1">User</Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Layout style={{padding: '0 24px 24px'}}>
              <Breadcrumb style={{margin: '16px 0'}}>
              </Breadcrumb>
              <Content
                className="site-layout-background"
                style={{
                  padding: 24,
                  margin: 0,
                  minHeight: 280,
                }}
              >
                  <PrivateRoute exact path="/user" Component={Form} />

              </Content>
            </Layout>
          </Layout>
        </Layout>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
