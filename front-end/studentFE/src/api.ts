import axios from "axios";

axios.defaults.baseURL = "https://5a65-123-16-236-131.ap.ngrok.io/api/v1";

export function getExam() {
  const id = localStorage.getItem("id");
  return axios.get(`/reexaminations?studentId=${id}`);
}
export function getPayment() {
  return axios.post(`/payment`, {
    price: 1,
    currency: "USD",
    method: "paypal",
    intent: "sale",
    description: "abc",
  });
}
export function getSem() {
  return axios.get(`/semesters`);
}
export function getSubject(semesterId: number) {
  const id = localStorage.getItem("id");
  return axios.get(`subjects?studentId=${id}&semesterId=${semesterId}`);
}
export function getStudyClass(semesterId: number, subjectid: number) {
  const id = localStorage.getItem("id");
  return axios.get(
    `subjects/${subjectid}/study_class?semesterId=${semesterId}&studentId=${id}`
  );
}

export function postReexam(data: any) {
  const id = localStorage.getItem("id");
  return axios.post(
    `reexaminations?semesterId=${data.semesterId}&subjectId=${data.subjectId}&studentId=${id}`
  );
}

// export const exam = [
//   {
//     id: 1,
//     status: "ACCEPT",
//     point: 0,
//     submit_time: "2021-07-05",
//     transcriptLine: {
//       id: 15,
//       study_class: {
//         id: 1,
//         name: "Nhóm 2",
//         room: "601-A2",
//         shift: 1,
//         subject_semester: {
//           id: 1,
//           semester: {
//             id: 1,
//             year_start: 2021,
//             semester_num: 2,
//           },
//           subject: {
//             id: 1,
//             name: "Đảm bảo chất lượng phần mềm",
//             subjectCode: "INT1416",
//           },
//         },
//       },
//       student: {
//         id: 1,
//         name: "Lê Anh",
//         student_code: "B18DCCN666",
//         mail: "anhl@ptit.edu.vn",
//         phone: null,
//         password: "1",
//       },
//       reexamination_deadline: "2021-05-28",
//       transcript_item: [
//         {
//           id: 1,
//           subjectPoint: {
//             weight: 10,
//             id: 1,
//             point: {
//               id: 1,
//               name: "Chuyên cần",
//             },
//             subject: {
//               id: 1,
//               name: "Đảm bảo chất lượng phần mềm",
//               subjectCode: "INT1416",
//             },
//           },
//           point: 9,
//         },
//         {
//           id: 6,
//           subjectPoint: {
//             weight: 70,
//             id: 4,
//             point: {
//               id: 6,
//               name: "Cuối kỳ",
//             },
//             subject: {
//               id: 1,
//               name: "Đảm bảo chất lượng phần mềm",
//               subjectCode: "INT1416",
//             },
//           },
//           point: 4,
//         },
//         {
//           id: 11,
//           subjectPoint: {
//             weight: 10,
//             id: 2,
//             point: {
//               id: 2,
//               name: "Bài tập lớn",
//             },
//             subject: {
//               id: 1,
//               name: "Đảm bảo chất lượng phần mềm",
//               subjectCode: "INT1416",
//             },
//           },
//           point: 4,
//         },
//       ],
//     },
//   },
// ];
